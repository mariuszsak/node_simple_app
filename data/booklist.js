import Book from '../model/Book.js';

export const booklist = {
    pan_tadeusz: new Book('Pan Tadeusz', 'Adam Mickiewicz'),
    wesele: new Book('Wesele', 'Stanislaw Wyspianski'),
    krzyzacy: new Book('Krzyzacy', 'Henryk Sienkiewicz'),
    lalka: new Book('Lalka', 'Boleslaw Prus'),
    iliada: new Book('Iliada', 'Homer'),
    rok_1984: new Book('Rok 1984', 'George Orwell'),
    tango: new Book('Tango', 'Sławomir Mrożek'),
    zbrodnia_i_kara: new Book('Zbrodnia i kara', 'Fiodor Dostojewski'),
    potop: new Book('Potop', 'Henryk Sienkiewicz'),
    z_legend_dawnego_egiptu: new Book('Z legend dawnego Egiptu', 'Bolesław Prus'),
    gloria_victis: new Book('Gloria victis', 'Eliza Orzeszkowa'),
    makbet: new Book('Makbet', 'William Szekspir'),
    romeo_i_julia: new Book('Romeo i Julia', 'William Szekspir'),
    skapiec: new Book('Skąpiec', 'Molier'),
    boska_komedia: new Book('Boska komedia', 'Dante Alighieri'),
    antygona: new Book('Antygona', 'Sofokles'),
    maly_ksiaze: new Book('Mały Książę', 'Antonie de Saint Exupéry'),
    dywizjon_303: new Book('Dywizjon 303', 'Arkady Fiedler'),
    zemsta: new Book('Zemsta', 'Aleksander Fredro'),
    dziady: new Book('Dziady', 'Adam Mickiewicz'),
    kamienie_na_szaniec: new Book('Kamienie na szaniec', 'Aleksander Kamiński'),
    balladyna: new Book('Balladyna', 'Juliusz Słowacki')
};

export default booklist;
