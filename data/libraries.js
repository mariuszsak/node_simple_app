import Library from '../model/Library.js';
import cities from './cities.js';

export const libraries = {
    warszawa1: new Library('Biblioteka Publiczna nr 1', cities.warszawa.name),
    warszawa2: new Library('Biblioteka Publiczna nr 2', cities.warszawa.name),
    krakow1: new Library('Biblioteka Publiczna nr 1', cities.krakow.name),
    wroclaw1: new Library('Biblioteka Publiczna nr 1', cities.wroclaw.name),
    lodz1: new Library('Biblioteka Publiczna nr 1', cities.lodz.name)
};

export default libraries;
