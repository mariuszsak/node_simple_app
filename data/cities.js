import City from '../model/City.js';

export const cities = {
    warszawa: new City('Warszawa'),
    krakow: new City('Kraków'),
    wroclaw: new City('Wrocław'),
    lodz: new City('Łódź')
};

export default cities;
