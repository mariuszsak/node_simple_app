import User from '../model/User.js';

export const users = {
    user1: new User('Adam Kowalski'),
    user2: new User('Anna Nowak'),
    user3: new User('Tomasz Kwiatkowski'),
    user4: new User('Jerzy Zaniewski'),
    user5: new User('Zofia Majewska'),
    user6: new User('Stefan Zając')
};

export default users;
