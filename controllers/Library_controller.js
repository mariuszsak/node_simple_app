import BorrowedBooks from '../model/BorrowedBooks.js';
import AddedBook from '../model/AddedBook.js';

class Library_controller {
    borrowedBooks = new BorrowedBooks();
    addedBook = new AddedBook();

    addUserToLibrary(object, username) {
        object.users.push(username);
    }

    addBookToLibrary(object, bookname, amount) {
        object.books.push(this.addedBook.addBookToLibrary(bookname, amount));
    }

    putBorrowedBook(object, bookname, username) {
        object.books.map(o => o.book).map(b => b.title).find(t => {
            if (t === bookname.title) {
                object.borrowedBookList.push(this.borrowedBooks.borrowBook(bookname, username));

            } else {
                console.log('Book ' + bookname.title + ' not found in ' + object.name);
            }
        });
    }
}

export default Library_controller;
