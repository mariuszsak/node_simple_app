import users from './data/users.js';
import libraries from './data/libraries.js';
import Library_controller from './controllers/Library_controller.js';
import {booklist} from './data/booklist.js';

class Application {
    initializeAllLibraries() {
        let libData = new Library_controller();
        libData.addUserToLibrary(libraries.warszawa1, users.user1);
        libData.addUserToLibrary(libraries.warszawa1, users.user3);
        libData.addUserToLibrary(libraries.warszawa2, users.user2.userName);
        libData.addUserToLibrary(libraries.krakow1, users.user4.userName);
        libData.addUserToLibrary(libraries.wroclaw1, users.user5.userName);
        libData.addUserToLibrary(libraries.lodz1, users.user6.userName);

        libData.addBookToLibrary(libraries.warszawa1, booklist.potop, 2);
        libData.addBookToLibrary(libraries.warszawa1, booklist.balladyna, 3);
        libData.putBorrowedBook(libraries.warszawa1, booklist.balladyna, users.user1);
    }
}

export default Application;
