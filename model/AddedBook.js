class AddedBook {
    constructor(book, amount) {
        this.book = book;
        this.amount = amount;
    }

    addBookToLibrary(book, amount) {
        return new AddedBook(book, amount);
    }
}

export default AddedBook;
