class Library {
    users = [];
    books = [];
    borrowedBookList = [];

    constructor(name, city) {
        this.name = name;
        this.city = city;
    }
}

export default Library;
