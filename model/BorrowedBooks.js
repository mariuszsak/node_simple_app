class BorrowedBooks {
    constructor(book, user) {
        this.book = book;
        this.user = user;
    }

    borrowBook(book, user) {
        return new BorrowedBooks(book, user);
    }
}

export default BorrowedBooks;
