import Application from './application.js';
import libraries from './data/libraries.js';

function run() {
    let app = new Application();
    app.initializeAllLibraries();
    // console.log('All libraries:');
    // console.log(libraries);
    // console.log('--------------------------------------------------------------------');
    console.log('Library: ' + libraries.warszawa1.name);
    console.log(libraries.warszawa1);
    console.log(libraries.warszawa1.books);
    console.log(libraries.warszawa1.borrowedBookList);
}

run();
